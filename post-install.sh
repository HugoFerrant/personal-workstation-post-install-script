#!/bin/bash

# update & upgrade
sudo apt update
sudo apt upgrade -y
sudo apt autoremove -y
sudo apt autoclean -y



# install apt packages

## pre-accept MS eula (installed below)
echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | sudo debconf-set-selections
## import yarn apt repository and key 
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list


sudo apt install -y \
 ubuntu-restricted-extras \
 moreutils \
 git \
 curl \
 xclip \
 gnome-software \
 gnome-tweak-tool \
 gufw \
 materia-gtk-theme \
 yarn \
 jq \
 inotify-tools \
 zsh \
 fonts-powerline \
 terminator


# font cache
sudo fc-cache -fv

# install nvm
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | bash
#todo: add lines to zshrc



# install 1password gui (source: https://support.1password.com/install-linux/)
curl -sS https://downloads.1password.com/linux/keys/1password.asc | sudo gpg --dearmor --output /usr/share/keyrings/1password-archive-keyring.gpg
echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/1password-archive-keyring.gpg] https://downloads.1password.com/linux/debian/amd64 stable main' \
  | sudo tee /etc/apt/sources.list.d/1password.list
mkdir -p /etc/debsig/policies/AC2D62742012EA22/
curl -sS https://downloads.1password.com/linux/debian/debsig/1password.pol | sudo tee /etc/debsig/policies/AC2D62742012EA22/1password.pol
mkdir -p /usr/share/debsig/keyrings/AC2D62742012EA22
curl -sS https://downloads.1password.com/linux/keys/1password.asc | sudo gpg --dearmor --output /usr/share/debsig/keyrings/AC2D62742012EA22/debsig.gpg
sudo apt update && sudo apt install 1password



# install 1password cli (source: https://support.1password.com/command-line-getting-started/#set-up-the-command-line-tool/)
onePasswordVersion="1.12.2"
osArchi=$(dpkg --print-architecture)
OPurl="https://cache.agilebits.com/dist/1P/op/pkg/v${onePasswordVersion}/op_linux_${osArchi}_v${onePasswordVersion}.zip"
wget $OPurl -O temp.zip && unzip temp.zip && rm -f temp.zip
gpg --receive-keys 3FEF9748469ADBE15DA7CA80AC2D62742012EA22
gpg --verify op.sig op
sudo mv op /usr/local/bin/
rm -f ./op.sig
op update
eval $(op signin https://my.1password.com hugoferrantperso@gmail.com) #will prompt for secret key, and master password



# Completly remove snap and snaps (source: https://pled.fr/?p=17052)
sudo snap remove $(snap list | awk '!/^Name|^core/ {print $1}')
snap remove snapd #shall be the last removed
sudo apt autoremove -y --purge snapd gnome-software-plugin-snap
sudo rm -rf \
  ~/snap \
  /snap \
  /var/snap \
  /var/lib/snapd \
  /var/cache/snapd
sudo apt-mark hold snapd



# install flatpak and flatpak apps
sudo apt install -y flatpak gnome-software-plugin-flatpak
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak install -y \
 flathub \
 com.spotify.Client \
 com.discordapp.Discord \
 com.axosoft.GitKraken \
 org.gtk.Gtk3theme.Materia-dark



# add apps to gnome favorites (in order)
favoritesAppsToAdd=(
  'firefox' \
  'org.gnome.Nautilus' \
  'terminator' \
  'com.axosoft.GitKraken.UrlHandler' \
  'com.spotify.Client' \
  'com.discordapp.Discord' \
) # XXX in array = XXX.desktop file
gsettings set org.gnome.shell favorite-apps "['${favoritesAppsToAdd[0]}.desktop']" #override the current settings with the first element
favoritesAppsToAdd=("${favoritesAppsToAdd[@]:1}") #removed the 1st element already added
for i in "${favoritesAppsToAdd[@]}"; do
  gsettings set org.gnome.shell favorite-apps "$(gsettings get org.gnome.shell favorite-apps | sed s/.$//), '$i.desktop']"
done



mkdir $HOME/utils # used by a few steps below



# install gnome extensions from sync

## Alternative install method by cloning and building the sources (atm not working because of missing deps):
# cd $HOME/utils
# git clone https://github.com/oae/gnome-shell-extensions-sync.git
# cd ./gnome-shell-extensions-sync
# nvm install --lts
# yarn install
# sudo apt install -y \
#     mutter \
#     gobject-introspection
# yarn build
# ln -s "$PWD/dist" "$HOME/.local/share/gnome-shell/extensions/extensions-sync@elhan.io"

## install the "extensions-sync" extension
mkdir /tmp/some_tmp_dir
cd /tmp/some_tmp_dir
curl -sS https://extensions.gnome.org/extension-data/extensions-syncelhan.io.v12.shell-extension.zip > file.zip
uuid=$(unzip -c file.zip metadata.json | grep uuid | cut -d \" -f4)
unzip -q file.zip -d ~/.local/share/gnome-shell/extensions/$uuid/
rm -f file.zip
cd ..
rm -rf some_tmp_dir

## set gist id and token
gitlabTokenForGnomeExtensionsSyncOPitemId=xwpkwmlvzjjuolvnhgtc5bmcaq
gitlabTokenForGnomeExtensionsSync=$(op get item "${gitlabTokenForGnomeExtensionsSyncOPitemId}" --fields password)
gitlabTokenForGnomeExtensionsSyncGistId=$(op get item "${gitlabTokenForGnomeExtensionsSyncOPitemId}" --fields username)
dconf write /org/gnome/shell/extensions/extensions-sync/provider "'Gitlab'"
dconf write /org/gnome/shell/extensions/extensions-sync/gitlab-snippet-id "'${gitlabTokenForGnomeExtensionsSyncGistId}'"
dconf write /org/gnome/shell/extensions/extensions-sync/gitlab-user-token "'${gitlabTokenForGnomeExtensionsSync}'"

## enable the extension
gnome-shell-extension-tool -e $uuid
#enabling runs the sync once, so do it after the dconf should be perfect, if dconf is OK with setting

## force restart gnome-shell
killall -3 gnome-shell

## bonus: install driver for possible future installations from browser
sudo apt install chrome-gnome-shell



# VS Code Install
wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
sudo install -o root -g root -m 644 packages.microsoft.gpg /etc/apt/trusted.gpg.d/
sudo sh -c 'echo "deb [arch=amd64,arm64,armhf signed-by=/etc/apt/trusted.gpg.d/packages.microsoft.gpg] https://packages.microsoft.com/repos/code stable main" > /etc/apt/sources.list.d/vscode.list'
rm -f packages.microsoft.gpg

sudo apt install apt-transport-https
sudo apt update
sudo apt install code

# adding to favorite
gsettings set org.gnome.shell favorite-apps "$(gsettings get org.gnome.shell favorite-apps | sed s/.$//), 'code.desktop']"


# vscode settings sync init
code --install-extension shan.code-settings-sync

syncLocalSettingsFile=/home/hugo/.config/Code/User/syncLocalSettings.json
settingsFile=/home/hugo/.config/Code/User/settings.json

code

while [ ! -f "$syncLocalSettingsFile" ]
do
    inotifywait -qqt 2 -e create -e moved_to "$(dirname $syncLocalSettingsFile)"
done

killall code

githubTokenOPitemId=3pxole2ejiku7puoldgijckgo4
githubTokenFromOP=$(op get item "${githubTokenOPitemId}" --fields password)
cat "$syncLocalSettingsFile" \
    | jq --tab --arg githubTokenFromOP "$githubTokenFromOP" '."token" |= $githubTokenFromOP' \
    | sponge "$syncLocalSettingsFile"


echo "{}" >> "$settingsFile"
githubGistIdFromOP=$(op get item "${githubTokenOPitemId}" --fields "gist id")
cat "$settingsFile" \
    | jq --tab --arg githubGistIdFromOP "$githubGistIdFromOP" '."sync.gist" |= $githubGistIdFromOP' \
    | sponge "$settingsFile"

cat "$settingsFile" \
    | jq --tab '."sync.autoDownload" |= true' \
    | sponge "$settingsFile"

code
while [ $(cat "$syncLocalSettingsFile" | jq '.lastDownload') = null ]
do
    echo "waiting 4s for download/install to finish"
    sleep 4
done
killall code



# grub theme
cd $HOME/utils/
git clone https://github.com/vinceliuice/grub2-themes.git
cd grub2-themes/
sudo ./install.sh -b -t slaze



# ssh keygen
sshKeyComment=$(whoami)@$(hostname) #it's already the default but just in case
keyPath="$HOME/.ssh/id_rsa"
ssh-keygen -t rsa -b 4096 -q -f "$keyPath" -N "" -C "$sshKeyComment"

# add key to github
githubUser=HugoFerrant
githubTokenForKeysOPitemId=aajzgwe6z5rrivl7iggxqdnrzm
githubTokenForKeys=$(op get item "${githubTokenForKeysOPitemId}" --fields password)
curl --silent -u "$githubUser:$githubTokenForKeys" --data "{\"title\": \"$sshKeyComment\", \"key\": \"$(cat $keyPath.pub)\"}" \
    https://api.github.com/user/keys \
    > /dev/null #to avoid output

# add key to gitlab
gitlabTokenForKeysOPitemId=lexk2q5gukrxd5ukru5zouocrm
gitlabTokenForKeys=$(op get item "${gitlabTokenForKeysOPitemId}" --fields password)
curl --silent -H 'Content-Type: application/json' --data '{"title":"'"$sshKeyComment"'","key":"'"$(cat $keyPath.pub)"'"}' \
    https://gitlab.com/api/v4/user/keys?private_token="$gitlabTokenForKeys" \
    > /dev/null #to avoid output



# retrieve dotfiles bare repo
REPO=git@gitlab.com:HugoFerrant/my-dotfiles.git
git clone --bare $REPO $HOME/.dotfiles
git --git-dir=$HOME/.dotfiles/ config --local status.showUntrackedFiles no
git --git-dir=$HOME/.dotfiles --work-tree=$HOME checkout -f #to force overwrite



### wip - zsh (don't forget : nvm, ssh-agent, ...)

git clone --recursive https://github.com/sorin-ionescu/prezto.git "${ZDOTDIR:-$HOME}/.zprezto"
setopt EXTENDED_GLOB
for rcfile in "${ZDOTDIR:-$HOME}"/.zprezto/runcoms/^README.md(.N); do
  ln -s "$rcfile" "${ZDOTDIR:-$HOME}/.${rcfile:t}"
done


# install virtualbox
wget -q -O- http://download.virtualbox.org/virtualbox/debian/oracle_vbox_2016.asc | sudo apt-key add -
echo "deb [arch=amd64] http://download.virtualbox.org/virtualbox/debian $(lsb_release -sc) contrib" | sudo tee /etc/apt/sources.list.d/virtualbox.list
sudo apt update
apt-cache madison virtualbox
sudo apt install virtualbox
sudo usermod -G vboxusers -a $USER



# install docker
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo \
"deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
$(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt update
sudo apt install docker-ce docker-ce-cli containerd.io docker-compose -y
sudo usermod -aG docker $USER
sudo service docker start



# clean up!
sudo apt update
sudo apt upgrade -y
sudo apt autoremove -y
sudo apt autoclean -y
flatpak update
sudo flatpak repair
flatpak uninstall -y --unused

